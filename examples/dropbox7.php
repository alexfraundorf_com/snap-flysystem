<?php
// php7 dropbox example/test

// dropbox api token
$token = 'secretToken';

require('vendor/autoload.php');

$Filesystem = new \Snap\Flysystem\Dropbox7($token);

require('test_include.php');
