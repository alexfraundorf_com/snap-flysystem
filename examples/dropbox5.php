<?php
// php5 dropbox example/test

// dropbox api token
$token = 'secretToken';

require_once('vendor/autoload.php');

$Filesystem = new \Snap\Flysystem\Dropbox5($token);

require('test_include.php');
