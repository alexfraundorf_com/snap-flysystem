<?php
// test include used by examples

// delete test data if it exists (reset test environment)
$Filesystem->deleteDirectory('test1', false);
if(is_file('copied_file.txt')) {
    unlink('copied_file.txt');
}
$Filesystem->deleteDirectory('copy_of_test2', false);


// make a nested directory
$Filesystem->createDirectory('test1/test2/test3/test4');
if($Filesystem->has('test1/test2/test3/test4')) {
    echo 'created directory<br>';
}
else {
    echo '<b>failed to create directory</b><br>';
}

// check getFilePath()
$Filesystem->setFilePath('test1/test2/test3');
if($Filesystem->getFilePath() === 'test1/test2/test3') {
    echo 'method setFilePath() and getFilePath() work<br>';
}
else {
    echo '<b>method setFilePath() or getFilePath() are not working</b><br>File '
    . 'path should be test1/test1/test3 but is ' . $Filesystem->getFilePath() . '<br>';
}
// reset file path
$Filesystem->setFilePath('');

// delete a directory
$Filesystem->deleteDirectory('test1/test2/test3/test4');
if(! $Filesystem->has('test1/test2/test3/test4')) {
    echo 'deleted directory<br>';
}
else {
    echo '<b>failed to delete directory</b><br>';
}

// create file 1
$Filesystem->write('test1/test2/text_test1.txt', 'This is a test file.  Hooray!');
if($Filesystem->has('test1/test2/text_test1.txt')) {
    echo 'created file 1<br>';
}
else {
    echo '<b>failed to create file 1</b><br>';
}

// create file 2
$Filesystem->write('test1/text_test2.txt', 'This is a test file.  Hooray!');
if($Filesystem->has('test1/text_test2.txt')) {
    echo 'created file 2<br>';
}
else {
    echo '<b>failed to create file 2</b><br>';
}

// create file 3
$Filesystem->setFilePath('test1/test2/test3');
$Filesystem->write('text_test3.txt', 'This is a test file.  Hooray!');
$Filesystem->setFilePath('');
if($Filesystem->has('test1/test2/test3/text_test3.txt')) {
    echo 'created file 3<br>';
}
else {
    echo '<b>failed to create file 3</b><br>';
}

// copy a file
$Filesystem->copy('test1/test2/text_test1.txt', 'test1/test2/test3/copy_of_text_test1.txt');
if($Filesystem->has('test1/test2/text_test1.txt') 
        && $Filesystem->has('test1/test2/test3/copy_of_text_test1.txt')) {
    echo 'copied file<br>';
}
else {
    echo '<b>failed to copy file</b><br>';
}

// copy a directory and its contents
$Filesystem->copy('test1/test2/test3', 'test1/test2/test3_copy');
if($Filesystem->has('test1/test2/test3_copy') 
        && $Filesystem->has('test1/test2/test3_copy/text_test3.txt')
        && $Filesystem->has('test1/test2/test3')) {
    echo 'copied directory<br>';
}
else {
    echo '<b>failed to copy directory</b><br>';
}

// move a file
$Filesystem->move('test1/test2/text_test1.txt', 'test1/test2/test3/text_test1.txt');
if(! $Filesystem->has('test1/test2/text_test1.txt') 
        && $Filesystem->has('test1/test2/test3/text_test1.txt')) {
    echo 'moved file<br>';
}
else {
    echo '<b>failed to move file</b><br>';
}

// move a directory and its contents
$Filesystem->move('test1/test2/test3', 'test1/test3_moved');
if(! $Filesystem->has('test1/test2/text3') 
        && $Filesystem->has('test1/test3_moved') 
        && $Filesystem->has('test1/test3_moved/copy_of_text_test1.txt')) {
    echo 'moved directory<br>';
}
else {
    echo '<b>failed to move directory</b><br>';
}

// copy a file from the remote file system
$Filesystem->copyFileFromFileSystem('test1/test2/test3_copy/copy_of_text_test1.txt', 'copied_file.txt');
if(is_file('copied_file.txt')) {
    echo 'copied file from remote file system to local<br>';
}
else {
    echo '<b>failed to copy file from remote file system to local</b><br>';
}

// copy a file to the remote file system
try {
    $Filesystem->setFilePath();
    $Filesystem->copyFileToFileSystem('/home/vagrant/Code/dropbox_test2/examples/copied_file.txt', 'test1/test2/file_copied_from_local.txt');
    if($Filesystem->has('test1/test2/file_copied_from_local.txt')) {
        echo 'copied file from local to remote file system<br>';
    }
    else {
        echo '<b>failed to copy file from local to remote file system</b><br>';
    }
} catch (\BadMethodCallException $ex) {
    echo '<b>failed to copy file from local to remote file system: ' . $ex->getMessage() . '</b><br>';
}

// copy a directory (and its contents) from the remote file system
$Filesystem->copyDirectoryFromFileSystem('test1/test2', 'copy_of_test2');
if(is_dir('copy_of_test2') && is_file('copy_of_test2/test3_copy/text_test3.txt')) {
    echo 'copied directory from remote file system<br>';
}
else {
    echo '<b>failed to copy directory from remote file system</b><br>';
}

// copy a directory (and its contents) to the remote file system
try {
    $Filesystem->copyDirectoryToFileSystem('copy_of_test2', 'test1/copy_of_test2');
    if($Filesystem->has('test1/copy_of_test2') && $Filesystem->has('test1/copy_of_test2/test3_copy/text_test3.txt')) {
        echo 'copied directory from local file system<br>';
    }
    else {
        echo '<b>failed to copy directory from local file system</b><br>';
    } 
} catch (\BadMethodCallException $ex) {
    echo '<b>failed to copy directory from local file system: ' . $ex->getMessage() . '</b><br>';
}

