<?php
// local file system example/test

require('vendor/autoload.php');

$Filesystem = new \Snap\Flysystem\Local(__DIR__);

require('test_include.php');
