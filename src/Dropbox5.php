<?php
/**
 * Wrapper class for communicating with the Dropbox API 
 *  (using thephpleague/flysystem) using PHP 5.x.
 * 
 * This class uses version 2 of the Dropbox API and works on PHP5 or higher.
 * Note: If you are using PHP7, please use Dropbox7.php instead.
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 07/11/2017 0.1.0
 * @since 06/29/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com/adapter/dropbox/
 * @link https://github.com/srmklive/flysystem-dropbox-v2
 * 
 * Before being able to use this class, you need to create an app and get an access token from Dropbox.
 * @link https://www.dropbox.com/developers/apps/create
 */
namespace Snap\Flysystem;

// import classes
use League\Flysystem\Filesystem as Filesystem;
use Srmklive\Dropbox\Client\DropboxClient as Client;
use Srmklive\Dropbox\Adapter\DropboxAdapter as Adapter;

class Dropbox5 extends FlysystemAbstract {
    
    
    /**
     *
     * @var string copy of dropbox auth token
     */
    protected $auth_token;
    
    
    /**
     *
     * @var adapter client object
     */
    protected $Client;
    
    
    /**
     * Constructor performs set up
     * 
     * @param string $auth_token dropbox auth token
     * @param string $file_path the relative file path inside the dropbox folder for the application
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function __construct($auth_token, $file_path = '') {
        // copy the auth token to the object
        $this->auth_token = (string) $auth_token;
        // create the client object
        $this->Client = new Client($auth_token);
        // set the file path and create the file system object
        $this->setFilePath($file_path);
    }
    
    
    /**
     * Build and return a league flysystem Filesystem object for a specified directory within the connected dropbox account.
     * 
     * @param string $file_path optional path to the desired directory within the dropbox account
     * @return \League\Flysystem\Filesystem
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function fileSystemFactory($file_path = '') {
        if(!$this->Client) {
            throw new \UnexpectedValueException('There is no client object set '
                    . 'as a property of ' . $this->get_class());
        }
        return new Filesystem(new Adapter($this->Client, $file_path));
    }
    

    /**
     * Return an array of files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFiles($only_return_names = false) {
        $contents = $this->listContents();
        $files = [];
        if(!$contents) {
            return $files;
        }
        foreach($contents as $file) {
            // skip dropbox.com urls files
            if(strpos($file['path'], 'www.dropbox.com') !== false) {
                continue;
            }
            if($file['type'] === 'file') {
                if($only_return_names) {
                    $files[] = $file['path'];
                }
                else {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }
    
    
    /**
     * Override for Filesystem method because the current Dropbox API does not 
     *  support returning mime types.
     * 
     * @param string $path
     * @throws \ErrorException
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function getMimetype($path) {
        throw new \ErrorException('The Dropbox API version 2 does not support '
                . 'mime types.');
    }    

    
    /**
     * Copy a file from a local path to the file system.
     * 
     * This method does not work using the PHP5 library for an unknown reason.
     * 
     * @throws \BadMethodCallException
     * @version 0.1.0 07/11/2017
     * @since 0.1.0 07/11/2017
     */
    public function copyFileToFileSystem($local_file_path, $remote_file_name, $overwrite = true) {
        // for an unknown reason this does not work.  file_get_contents reads 
        // an empty string from the local file.
        throw new \BadMethodCallException('Sorry the method copyFileToFileSystem() '
                . 'does not work with the PHP5 library.  You will need to use '
                . 'PHP7 and the PHP7 library, Dropbox7');
    }
    
    
    /**
     * Recursively copy a directory and all of its contents to the file system.
     * 
     * This method does not work using the PHP5 library for an unknown reason.
     * 
     * @throws \BadMethodCallException
     * @version 0.1.0 07/11/2017
     * @since 0.1.0 07/11/2017
     */
    public function copyDirectoryToFileSystem($path_to_local_directory, $path_to_remote_directory) {
        // for an unknown reason this does not work.  file_get_contents reads 
        // an empty string from the local file which causes copyFileToFileSystem() 
        // to fail.
        throw new \BadMethodCallException('Sorry the method copyFileToFileSystem() '
                . 'does not work with the PHP5 library.  You will need to use '
                . 'PHP7 and the PHP7 library, Dropbox7');
    }

    
}
