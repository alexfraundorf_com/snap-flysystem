<?php
/**
 * An abstract class to be extended by any class using League\Flysystem to add 
 * extra useful functionality when working with a file system.
 * 
 * This class adds additional functionality to and is interchangeable with 
 * League\Flysystem\Filesystem (implements its interface); however, there are 
 * additional optional parameters and different return types for some methods 
 * such as rename(), copy() and addPlugin().
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 07/09/2017 0.1.0
 * @since 06/29/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com
 * @link https://github.com/thephpleague/flysystem/tree/master/src
 */
namespace Snap\Flysystem;

// import classes
use League\Flysystem\FileNotFoundException as FileNotFoundException;

abstract class FlysystemAbstract implements FlysystemInterface, \League\Flysystem\FilesystemInterface {

    
    /**
     *
     * @var League\Flysystem\FilesystemInterface object
     */
    protected $FileSystem;
    
    /**
     *
     * @var string the file path within the ftp account file system
     */
    protected $file_path;
        
    /**
     *
     * @var array of image file extensions (used by getImageFiles())
     */
    protected $image_extensions = [
        'jpg',
        'jpeg',
        'gif',
        'png',
        'tif',
        'bmp',
    ];

    
    /**
     * **********************************************************************************************
     * Additional functionality
     * **********************************************************************************************
     */

    
    /**
     * Return the filesystem object.
     * 
     * @return League\Flysystem\FilesystemInterface
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFileSystemObject() {
        return $this->FileSystem;
    }
    
    
    /**
     * Return the adapter object.
     * 
     * @return League\Flysystem\AdapterInterface
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getAdapterObject() {
        return $this->FileSystem->getAdapter();
    }
        
    
    /**
     * Sets/updates the file path and the object's FileSystem object.
     * 
     * @param string $file_path new file path (relative to the file system)
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException if the file path is not set correctly
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function setFilePath($file_path = '') {
        $this->file_path = (string) $file_path;
        $this->FileSystem = $this->fileSystemFactory($file_path);
        return $this;
    }
    
    
    /**
     * Return the current file path.
     * 
     * @return string
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFilePath() {
        return (string) $this->file_path;
    }
    
    
    /**
     * Return an array of directories in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getDirectories($only_return_names = false) {
        $contents = $this->listContents();
        $directories = [];
        if(!$contents) {
            return $directories;
        }
        foreach($contents as $file) {
            if($file['type'] === 'dir') {
                if($only_return_names) {
                    $directories[] = $file['filename'];
                }
                else {
                    $directories[] = $file;
                }
            }
        }
        return $directories;
    }
    
    
    /**
     * Return an array of files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFiles($only_return_names = false) {
        $contents = $this->listContents();
        $files = [];
        if(!$contents) {
            return $files;
        }
        foreach($contents as $file) {
            if($file['type'] === 'file') {
                if($only_return_names) {
                    $files[] = $file['path'];
                }
                else {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }
    
    
    /**
     * Return an array of the image files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getImageFiles($only_return_names = false) {
        $contents = $this->listContents();
        $files = [];
        if(!$contents) {
            return $files;
        }
        foreach($contents as $file) {
            if($file['type'] === 'file' && in_array($file['extension'], $this->image_extensions)) {
                if($only_return_names) {
                    $files[] = $file['path'];
                }
                else {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }
    
    
    /**
     * Return a multi-dimensional array with sub-arrays of 'directories' and 'files' 
     *  containing an array of directories and files, respectively, within the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getDirectoriesAndFiles($only_return_names = false) {
        return [
            'directories' => $this->getDirectories($only_return_names),
            'files' => $this->getFiles($only_return_names),
        ];
    }
    
    
    /**
     * Return an array of details about a file/directory by its name or an empty 
     *  array if no match is found.
     * 
     * @param string $file_name
     * @return array
     * @throws \League\Flysystem\FileNotFoundException if file is not found
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFileDetails($file_name) {
        $file_name = $this->validatePath($file_name);
        $directory_path = $this->extractDirectoryPath($file_name);
        $contents = $this->listContents($directory_path);
        // remove the full directory path if needed
        if(! $this->FileSystem->has($directory_path . $file_name) && $directory_path) {
            $file_name = str_replace($directory_path, '', $file_name);
        } 
        foreach($contents as $file) {
            if($file['path'] === $file_name) {
                return $file;
            }
        }
        // no match 
        throw new FileNotFoundException('Could not find file: ' 
                . $this->getFilePath() . $file_name);
    }
    
    
    /**
     * Copy a file from the current file system file path to a local file path.
     * 
     * The object will be returned on success and an exception will be thrown if 
     *  there is an error.
     * 
     * @param string $remote_file_name the name of the file within the current file path
     * @param string $local_file_path local file path (tip: use an absolute path)
     *   If an absolute path is not used, it will be relative to the file's location.
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract
     * @throws \ErrorException
     * @throws \UnexpectedValueException
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 06/29/2017
     */
    public function copyFileFromFileSystem($remote_file_name, $local_file_path, $overwrite = true) {
        $remote_file_name = $this->validatePath($remote_file_name);
        if(!$this->FileSystem->has($remote_file_name)) {
            throw new \UnexpectedValueException('Source file in remote file system '
                    . '(' . $this->getFilePath() . $remote_file_name . ') does not '
                    . 'exist.');
        }
        $file_content = $this->FileSystem->read($remote_file_name);
        // check if the local copy already exists
        if(file_exists($local_file_path)) {
            if($overwrite) {
                unlink($local_file_path);
            }
            else {
                throw new \UnexpectedValueException('File can not be written to '
                        . 'local file system because the file already exists at (' 
                        . $local_file_path . ') and the $overwrite param is set '
                        . 'to false.');
            }
        }
        if(file_put_contents($local_file_path, $file_content) === false) {
            throw new \ErrorException('Failed to copy file (' . $remote_file_name 
                    . ') from file system folder (' . $this->getFilePath() 
                    . ') to local path (' . $local_file_path . ').');
        }
        return $this;
    }
    
    
    /**
     * Copy a file from a local path to the file system.
     * 
     * @param string $local_file_path local file path (tip: use an absolute path)
     *   If an absolute path is not used, it will be relative to the file's location.
     * @param string $remote_file_name the new name of the file being copied to 
     *  the remote file system
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem/FlysystemAbstract
     * @throws \UnexpectedValueException
     * @throws \ErrorException
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function copyFileToFileSystem($local_file_path, $remote_file_name, $overwrite = true) {
        if(!is_readable($local_file_path)) {
            throw new \UnexpectedValueException('Local file path (' 
                    . $local_file_path . ') does not exist or is not readable.');
        }
        $file_content = file_get_contents($local_file_path);
        if($file_content === false) {
            throw new \ErrorException('Failed to read content from file: ' . $local_file_path);
        }
        // check if the file already exists 
        if($this->FileSystem->has($remote_file_name)) {
            if($overwrite) {
                $this->FileSystem->delete($remote_file_name);
            }
            else {
                throw new \UnexpectedValueException('File can not be written to '
                        . 'remote file system because the file already exists at (' 
                        . $this->getFilePath() . $remote_file_name . ') and '
                        . '$overwrite param is set to false.');
            }
        }
        if($this->FileSystem->write($remote_file_name, $file_content) === false) {
            throw new \ErrorException('Failed to copy local file (' 
                    . $local_file_path . ') to remote file system folder (' 
                    . $this->getFilePath() . ') with the name (' . $remote_file_name . ').');
        }
        return $this;
    }
     
    
    /**
     * Create a directory.
     *
     * @param string $directory_name the name of the new directory
     *  Can be "test" or nested "test/test2/test3"
     * @param array  $config  an optional configuration array
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException if the directory was not created
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 06/29/2017
     */
    public function createDirectory($directory_name, array $config = []) {
        if(!$this->FileSystem->createDir($directory_name, $config)) {
            throw new \ErrorException('Failed to create directory: ' . $directory_name);
        }
        return $this;
    }
    
    
    /**
     * Delete a file on the file system by its name/path.
     * 
     * @param string $file_path
     * @param bool $throw_exception_if_file_not_found if true a 
     *  \League\Flysystem\FileNotFoundException will be thrown if the file is 
     *  not found
     * @return \Snap\Flysystem\FlysystemAbstract
     * @throws \League\Flysystem\FileNotFoundException if file can't be found
     * @throws \ErrorException of delete fails
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function deleteFile($file_path, $throw_exception_if_file_not_found = true) {
        $file_path = $this->validatePath($file_path, $throw_exception_if_file_not_found);
        if(! $throw_exception_if_file_not_found && ! $file_path) {
            return $this;
        }
        if(! $this->FileSystem->has($file_path)) {
            throw new FileNotFoundException('Can not delete '
                    . 'file because it could not be found: ' 
                    . $this->getFilePath() . $file_path);
        }
        if(! $this->FileSystem->delete($file_path)) {
            throw new \ErrorException('Failed to delete file sytem file: ' . $file_path);
        }    
        return $this;
    }
    
    
    /**
     * Delete a directory on the file system by its path.
     * 
     * @param string $directory_path (may be nested)
     * @param bool $throw_exception_if_directory_not_found if true a 
     *  \League\Flysystem\FileNotFoundException will be thrown if the file is 
     *  not found
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 06/29/2017
     */
    public function deleteDirectory($directory_path, $throw_exception_if_directory_not_found = true) {
        $directory_path = $this->validatePath($directory_path, $throw_exception_if_directory_not_found);
        if(! $throw_exception_if_directory_not_found && ! $directory_path) {
            return $this;
        }
        if(! $this->FileSystem->deleteDir($directory_path)) {
            throw new \ErrorException('Failed to delete file system directory: ' 
                    . $directory_path);
        }    
        return $this;
    }
    
    
    /**
     * Copy a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if copy fails
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 06/29/2017
     */
    public function copy($current_path, $new_path, $overwrite = true) {
        $current_path = $this->validatePath($current_path);
        if(! $this->FileSystem->has($current_path)) {
            throw new FileNotFoundException('Could not find '
                    . 'file/directory to copy: ' . $this->getFilePath() 
                    . $current_path);
        }
        if($this->FileSystem->has($new_path)) {
            if($overwrite) {
                $this->FileSystem->delete($new_path);
            }
            else {
                throw new \UnexpectedValueException('File/directory can not be '
                        . 'copied because the file/directory already exists '
                        . 'at (' . $this->getFilePath() . $new_path . ') and '
                        . '$overwrite param is set to false.');
            }
        }
        if(! $this->FileSystem->copy($current_path, $new_path)) {
            throw new \ErrorException('Failed to copy file system file/directory: ' 
                    . $current_path . ' to ' . $new_path);
        }    
        return $this;
    }
    
    
    /**
     * Move a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if move fails
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 06/29/2017
     */
    public function move($current_path, $new_path, $overwrite = true) {
        $current_path = $this->validatePath($current_path);
        if(! $this->FileSystem->has($current_path)) {
            throw new FileNotFoundException('Could not find '
                    . 'file/directory to copy: ' . $this->getFilePath() 
                    . $current_path);
        }
        if($this->FileSystem->has($new_path)) {
            if($overwrite) {
                $this->FileSystem->delete($new_path);
                $this->FileSystem->deleteDir($new_path);
            }
            else {
                throw new \UnexpectedValueException('File/directory can not be '
                        . 'copied because the file/directory already exists '
                        . 'at (' . $this->getFilePath() . $new_path . ') and '
                        . '$overwrite param is set to false.');
            }
        }
        if(! $this->FileSystem->rename($current_path, $new_path)) {
            throw new \ErrorException('Failed to move/rename file/directory: ' 
                    . $current_path . ' to ' . $new_path);
        }    
        return $this;
    }
    
    
    /**
     * Recursively copy a directory and all of its contents from the file system.
     * 
     * @param string $path_to_remote_directory path to the remote file system 
     *  directory to copy
     * @param string $path_to_local_directory destination path to copy to. 
     *  tip: use an absolute path
     * @param int $nesting the level of nested folders
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \ErrorException if a copy error occurs
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function copyDirectoryFromFileSystem($path_to_remote_directory, $path_to_local_directory, $nesting = 0) {
        // create the local directory if it doesn't exist
        if(! is_dir($path_to_local_directory)) {
            mkdir($path_to_local_directory, 0777, true);
        }
        // add an ending / to the remote directory if it does not exist
        if(substr($path_to_remote_directory, -1, 1) !== '/') {
            $path_to_remote_directory .= '/';
        }
        // cd into file system directory
        $this->setFilePath($path_to_remote_directory);
        $files = $this->getFiles(true);
        if($files) {
            foreach($files as $file_name) {
                $this->copyFileFromFileSystem($file_name, $path_to_local_directory 
                        . '/' . $file_name, true);
            }
        }
        $directories = $this->getDirectories(true);
        if($directories) {
            foreach($directories as $directory_name) {
                $directory_path = $path_to_local_directory . '/' . $directory_name;
                if(!is_dir($directory_path)) {
                    mkdir($directory_path);
                }
            }
            // recursion
            foreach($directories as $directory_name) {
                $new_remote_directory = $path_to_remote_directory . $directory_name;
                $new_local_directory = $path_to_local_directory . '/' . $directory_name;
                $this->copyDirectoryFromFileSystem($new_remote_directory, $new_local_directory, $nesting++);
            }
        }
        if($nesting === 0) {
            // rest the file path
            $this->setFilePath('');
        }        
        return $this;
    }
    
    
    /**
     * Recursively copy a directory and all of its contents to the file system.
     * 
     * @param string $path_to_local_directory destination path to copy from. 
     *  tip: use an absolute path
     * @param string $path_to_remote_directory path to the remote file system 
     *  directory destination
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \ErrorException if a copy error occurs
     * @version 0.1.0 07/11/2017
     * @since 0.1.0 06/29/2017
     */
    public function copyDirectoryToFileSystem($path_to_local_directory, $path_to_remote_directory) {
        if(! is_dir($path_to_local_directory)) {
            throw new FileNotFoundException('Local directory does not exist: ' 
                    . $path_to_local_directory);
        }
        // create the file system directory if it doesn't exist
        if(! $this->FileSystem->has($path_to_remote_directory)) {
            $this->createDirectory($path_to_remote_directory);
        }
        // cd into remote directory
        $this->setFilePath($path_to_remote_directory);
        // create a local file system
        $Local = new \Snap\Flysystem\Local($path_to_local_directory);
        // get a list of directories and files within this file system directory
        $files_and_directories = $Local->getDirectoriesAndFiles(true);
        $files = $files_and_directories['files'];
        if($files) {
            foreach($files as $file) {
                $local_file_path = $path_to_local_directory . '/' . $file;
                $remote_file_path = $path_to_remote_directory . '/' . $file;
                if(! is_file($local_file_path)) {
                    throw new \ErrorException('Incorrect file path built: ' . $local_file_path);
                }
                $this->copyFileToFileSystem($local_file_path, $remote_file_path);
            }
        }
        $directories = $files_and_directories['directories'];       
        if($directories) {
            foreach($directories as $directory_name) {
                $new_local_directory = $path_to_local_directory . '/' . $directory_name;
                $new_remote_directory = $path_to_remote_directory . '/' . $directory_name;
                $this->copyDirectoryToFileSystem($new_local_directory, $new_remote_directory);
            }
        }
        return $this;
    }
    
    
    /**
     * Validate a file/directory path by prepending the file path if there is 
     *  one and it is not already in the file path.
     * 
     * @param string $provided_path the path to normalize and validate
     * @param bool $throw_exception_if_file_not_found if true a 
     *  \League\Flysystem\FileNotFoundException will be thrown if the file is 
     *  not found
     * @return string|false the validated path if it is found and false if the 
     *  file is not found and $throw_exception_if_file_not_found is set to false
     * @throws \League\Flysystem\FileNotFoundException if path cannot be found
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 07/03/2017
     */
    protected function validatePath($provided_path, $throw_exception_if_file_not_found = true) {
        $path = trim($provided_path);
        if($this->FileSystem->has($path)) {
            //exit('1 path found: ' . $path);
            return $path;
        }
        if(substr(trim($path), 0, strlen($this->file_path)) !== $this->file_path) {
            $path =  '/' . $this->file_path . $path;
            if($this->FileSystem->has($path)) {
                //exit('2 path found: ' . $path);
                return $path;
            }
        }
        else {
            $path = '/' . $path;
            if($this->FileSystem->has($path)) {
                //exit('3 path found: ' . $path);
                return $path;
            }
        }
        // no valid path found
        if(! $throw_exception_if_file_not_found) {
            return false;
        }
        throw new FileNotFoundException('Could not find a valid path for: ' . $provided_path);
    }

    
    /**
     * Extract and return a directory path from a file path.
     * 
     * @param string $file_path
     * @return string
     * @version 0.1.0 07/03/2017
     * @since 0.1.0 07/03/2017
     */
    protected function extractDirectoryPath($file_path) {
        if(strpos($file_path, '/') === false) {
            return '';
        }
        $pieces = explode('/', $file_path);
        array_pop($pieces);
        $directory_path = implode('/', $pieces) . '/';        
        //exit($directory_path);
        return $directory_path;
    }
    
    
    /**
     * *************************************************************************
     * Alias methods 
     * *************************************************************************
     */
    
    
    /**
     * Alias of setFilePath().
     * 
     * Sets/updates the file path and the object's FileSystem object.
     * 
     * @param string $file_path new file path (relative to the file system's path) 
     *  Note: the path must end with a /
     * @return \Snap\Flysystem\FlysystemAbstract
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function cd($file_path = '/') {
        return $this->setFilePath($file_path);
    }

    
    /**
     * Alias of getFilePath().
     * 
     * Return the current file path (relative to the file system).
     * 
     * @return string
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function pwd() {
        return $this->getFilePath();
    }
    
    
    /**
     * Alias of listContents().
     * 
     * Returns an array of all items inside the current file path.  Each item 
     *  is an array with detailed information about the file, directory, etc.
     * 
     * @param string $directory The directory to list.
     * @param bool   $recursive Whether to list recursively.
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function ls($directory = '', $recursive = false) {
        return $this->listContents();
    }
    
    
    /**
     * Alias of createDir().
     * 
     * Create a (nested) directory in the current file system directory.
     * 
     * @param string $directory_name the name of the new directory
     *  Can be "test" or nested "test/test2/test3"
     * @param array  $config  an optional configuration array
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException if the directory was not created
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function mkdir($directory_name, array $config = []) {
        return $this->createDirectory($directory_name, $config);
    }
    
    
    /**
     * Alias for deleteFile().
     * 
     * Delete a file on the file system by its name/path.
     * 
     * @param string $file_path
     * @return \Snap\Flysystem\FlysystemAbstract
     * @throws \League\Flysystem\FileNotFoundException if file can't be found
     * @throws \ErrorException of delete fails
     * @version 0.1.0 06/30/2017
     * @since 0.1.0 06/29/2017
     */
    public function rm($file_path) {
        return $this->deleteFile($file_path);
    }
    
    
    /**
     * Alias for deleteDirectory().
     * 
     * Delete a directory on the file system by its path.
     * 
     * @param string $directory_path (may be nested)
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function rmdir($directory_path) {
        return $this->deleteDirectory($directory_path);
    }
    
    
    /**
     * Alias for copy().
     * 
     * Copy a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if copy fails
     * @version 0.1.0 06/30/2017
     * @since 0.1.0 06/29/2017
     */    
    public function cp($current_path, $new_path, $overwrite = true) {
        return $this->copy($current_path, $new_path);
    }
    
    
    /**
     * Alias for move().
     * 
     * Move a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if move fails
     * @version 0.1.0 06/30/2017
     * @since 0.1.0 06/29/2017
     */
    public function mv($current_path, $new_path) {
        return $this->move($current_path, $new_path);
    }
    
    
    /**
     * Rename a file or directory. Note: This is an alias for move().
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if move fails
     * @version 0.1.0 06/30/2017
     * @since 0.1.0 06/29/2017
     */
    public function rename($current_path, $new_path, $overwrite = true) {
        return $this->move($current_path, $new_path, $overwrite);
    }
    
    
    /**
     * *************************************************************************
     * Provide a wrapper for the Flysystem/Filesystem methods 
     * *************************************************************************
     */

    
    /**
     * Check whether a file exists.
     *
     * @param string $path
     * @return bool
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function has($path) {
        return $this->FileSystem->has($path);        
    }
    
    
    /**
     * Read a file.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return string|false The file contents or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function read($path) {
        return $this->FileSystem->read($path);
    }
    
    
    /**
     * Retrieves a read-stream for a path.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return resource|false The path resource or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function readStream($path) {
        return $this->FileSystem->readStream($path);
    }
    
    
    /**
     * Returns an array of all items inside the current file path.  Each item 
     *  is an array with detailed information about the file, directory, etc.
     *
     * @param string $directory The directory to list.
     * @param bool   $recursive Whether to list recursively.
     * @return array A list of file metadata.
     * @version 0.1.0 07/04/2017
     * @since 0.1.0 06/29/2017
     */
    public function listContents($directory = '', $recursive = false) {
        /*
        if(! $directory && $this->file_path) {
            $directory = $this->file_path;
        } 
        */       
        return $this->FileSystem->listContents($directory, $recursive);
    }   
    
    
    /**
     * Get a file's metadata.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return array|false The file metadata or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getMetadata($path) {
        return $this->FileSystem->getMetadata($path);
    }
    

    /**
     * Get a file's size.
     *
     * @param string $path The path to the file.
     * @return int|false The file size or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getSize($path) {
        return $this->FileSystem->getSize($path);
    } 
    
    
    /**
     * Get a file's mime-type.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return string|false The file mime-type or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getMimetype($path) {
        return $this->FileSystem->getMimetype($path);
    }    
    
    
    /**
     * Get a file's timestamp.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return string|false The timestamp or false on failure.
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 06/29/2017
     */
    public function getTimestamp($path) {
        $timestamp = $this->FileSystem->getTimestamp($path);
        if(! $timestamp) {
            $details = $this->getFileDetails($path);
            if(isset($details['timestamp'])) {
                return $details['timestamp'];
            }
            return false;
        }
    }  
    
    
    /**
     * Get a file's visibility.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return string|false The visibility (public|private) or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getVisibility($path) {
        return $this->FileSystem->getVisibility($path);
    }  
    
    
    /**
     * Write a new file.
     *
     * @param string $path     The path of the new file.
     * @param string $contents The file contents.
     * @param array  $config   An optional configuration array.
     * @throws \League\Flysystem\FileExistsException
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function write($path, $contents, array $config = []) {
        return (bool) $this->FileSystem->write($path, $contents, $config);
    }  
    
    
    /**
     * Write a new file using a stream.
     *
     * @param string   $path     The path of the new file.
     * @param resource $resource The file handle.
     * @param array    $config   An optional configuration array.
     * @throws \InvalidArgumentException If $resource is not a file handle.
     * @throws \League\Flysystem\FileExistsException
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function writeStream($path, $resource, array $config = []) {
        return (bool) $this->FileSystem->writeStream($path, $resource, $config);
    }    
    
    
    /**
     * Update an existing file.
     *
     * @param string $path     The path of the existing file.
     * @param string $contents The file contents.
     * @param array  $config   An optional configuration array.
     * @throws \League\Flysystem\FileNotFoundException
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function update($path, $contents, array $config = []) {
        return (bool) $this->FileSystem->update($path, $contents, $config);
    }    
    
    
    /**
     * Update an existing file using a stream.
     *
     * @param string   $path     The path of the existing file.
     * @param resource $resource The file handle.
     * @param array    $config   An optional configuration array.
     * @throws \InvalidArgumentException If $resource is not a file handle.
     * @throws \League\Flysystem\FileNotFoundException
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function updateStream($path, $resource, array $config = []) {
        return (bool) $this->FileSystem->updateStream($path, $resource, $config);
    }

    
    /**
     * Rename a file.
     *
     * @param string $path    Path to the existing file.
     * @param string $newpath The new path of the file.
     * @throws \League\Flysystem\FileExistsException   Thrown if $newpath exists.
     * @throws \League\Flysystem\FileNotFoundException Thrown if $path does not exist.
     * @return bool True on success, false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    /*
    public function rename($path, $newpath) {
        return $this->FileSystem->rename($path, $newpath);
    }
    */
    
    
    /**
     * Copy a file.
     *
     * @param string $path    Path to the existing file.
     * @param string $newpath The new path of the file.
     * @throws \League\Flysystem\FileExistsException   Thrown if $newpath exists.
     * @throws \League\Flysystem\FileNotFoundException Thrown if $path does not exist.
     * @return bool True on success, false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    /*
    public function copy($path, $newpath) {
        return $this->FileSystem->copy($path, $newpath);
    }
    */
    
    
    /**
     * Delete a file.
     *
     * @param string $path
     * @throws \League\Flysystem\FileNotFoundException
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function delete($path) {
        return (bool) $this->FileSystem->delete($path);
    }
    
    
    /**
     * Delete a directory.
     *
     * @param string $dirname
     * @throws \League\Flysystem\RootViolationException Thrown if $dirname is empty.
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function deleteDir($dirname) {
        return (bool) $this->FileSystem->deleteDir($dirname);
    }  
      
    
    /**
     * Create a directory.
     *
     * @param string $dirname The name of the new directory.
     * @param array  $config  An optional configuration array.
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function createDir($dirname, array $config = []) {
        return (bool) $this->FileSystem->createDir($dirname, $config);
    }

    
    /**
     * Set the visibility for a file.
     *
     * @param string $path       The path to the file.
     * @param string $visibility One of 'public' or 'private'.
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function setVisibility($path, $visibility) {
        return (bool) $this->FileSystem->setVisibility($path, $visibility);
    }

    
    /**
     * Create a file or update if exists.
     *
     * @param string $path     The path to the file.
     * @param string $contents The file contents.
     * @param array  $config   An optional configuration array.
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function put($path, $contents, array $config = []) {
        return (bool) $this->FileSystem->put($path, $contents, $config);
    }
    
    
    /**
     * Create a file or update if exists.
     *
     * @param string   $path     The path to the file.
     * @param resource $resource The file handle.
     * @param array    $config   An optional configuration array.
     * @throws \InvalidArgumentException Thrown if $resource is not a resource.
     * @return bool True on success, false on failure.
     * @version 0.1.0 07/01/2017
     * @since 0.1.0 06/29/2017
     */
    public function putStream($path, $resource, array $config = []) {
        return (bool) $this->FileSystem->putStream($path, $resource, $config);
    }   
    
    
    /**
     * Read and delete a file.
     *
     * @param string $path The path to the file.
     * @throws \League\Flysystem\FileNotFoundException
     * @return string|false The file contents, or false on failure.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function readAndDelete($path) {
        return $this->FileSystem->readAndDelete($path);
    }
    
    
    /**
     * Get a file/directory handler.
     *
     * @param string  $path    The path to the file.
     * @param \League\Flysystem\Handler $handler An optional existing handler to populate.
     * @return \League\Flysystem\Handler Either a file or directory handler.
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function get($path, \League\Flysystem\Handler $handler = null) {
        return $this->FileSystem->get($path, $handler);
    }
    
    
    /**
     * Register a plugin.
     *
     * @param \League\Flysystem\PluginInterface $plugin The plugin to register.
     * @return \Snap\Flysystem\FlysystemAbstract
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function addPlugin(\League\Flysystem\PluginInterface $plugin) {
        $this->FileSystem->addPlugin($plugin);
        return $this;
    }    

    
    /**
     * *************************************************************************
     * Abstract methods that must be implemented in extending class
     * *************************************************************************
     */
    

    /**
     * Build and return a League\Flysystem\Filesystem object for a specified 
     *  directory within the file system.
     * 
     * Note: This will vary depending on the adapter being used.
     * 
     * @param string $file_path optional path to the desired directory within the filesystem
     * @return \League\Flysystem\Filesystem
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    abstract public function fileSystemFactory($file_path = '/');

       
    
}