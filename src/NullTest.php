<?php
/**
 * Wrapper class for communicating with a null/testing file system (using 
 *  thephpleague/flysystem).
 * 
 * Note: This is for testing use only.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 07/01/2017 0.1.0
 * @since 07/01/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com/adapter/null-test/
 */
namespace Snap\Flysystem;

// import classes
use League\Flysystem\Filesystem as Filesystem;
use League\Flysystem\Adapter\NullAdapter as Adapter;

class NullTest extends FlysystemAbstract {
    
    
    /**
     * Constructor performs set up
     * 
     * @param string $file_path the file path inside the local file system
     *  You can use absolute or relative paths, but absolute is recommended.
     * @version 06/30/2017 0.1.0
     * @since 06/30/2017 0.1.0
     */
    public function __construct($file_path = '') {
        $this->setFilePath($file_path);
    }
    
    
    /**
     * Build and return a league flysystem Filesystem object for a specified 
     *  directory within the connected local file system.
     * 
     * @param string $file_path optional path to the desired directory within 
     *  the local file system
     * @return \League\Flysystem\Filesystem
     * @version 06/30/2017 0.1.0
     * @since 06/30/2017 0.1.0
     */
    public function fileSystemFactory($file_path = '') {
        return new Filesystem(new Adapter($file_path));
    }
    
    
}