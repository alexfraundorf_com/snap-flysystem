<?php
/**
 * Wrapper class for communicating with the local file system (using thephpleague/flysystem).
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 07/11/2017 0.1.0
 * @since 06/30/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com/adapter/local/
 */
namespace Snap\Flysystem;

// import classes
use League\Flysystem\Filesystem as Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

class Local extends FlysystemAbstract {
    
    
    /**
     *
     * @var string holder for the file path used at object creation.
     * Note: When calling setFilePath() you will be returned to this path.
     */
    protected $original_path;
    
    
    /**
     * Constructor performs set up
     * 
     * @param string $file_path the file path inside the local file system
     *  You can use absolute or relative paths, but absolute is recommended.
     *  If no path is provided the magic variable __DIR__ will be used.
     * @version 07/11/2017 0.1.0
     * @since 06/30/2017 0.1.0
     */
    public function __construct($file_path = '') {
        if($file_path === '') {
            $file_path = __DIR__;
        }
        $this->original_path = $file_path;
        $this->setFilePath($file_path);
    }
    
    
    /**
     * Build and return a league flysystem Filesystem object for a specified 
     *  directory within the connected local file system.
     * 
     * @param string $file_path optional path to the desired directory within 
     *  the local file system
     * @return \League\Flysystem\Filesystem
     * @version 07/04/2017 0.1.0
     * @since 06/30/2017 0.1.0
     */
    public function fileSystemFactory($file_path = '') {
        return new Filesystem(new Adapter($file_path));
    }
        
    
    /**
     * Sets/updates the file path and the object's FileSystem object.
     * 
     * This overrides the default method to preserve returning to the original 
     *  file path as the root of this file system.
     * 
     * @param string $file_path new file path (relative to the file system)
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException if the file path is not set correctly
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 07/09/2017
     */
    public function setFilePath($file_path = '') {
        if($file_path === '') {
            $file_path = $this->original_path;
        }
        $this->file_path = (string) $file_path;
        $this->FileSystem = $this->fileSystemFactory($file_path);
        return $this;
    }
    
    
    
    public function copy($current_path, $new_path, $overwrite = true) {
        if(is_file($current_path)) {
            return parent::copy($current_path, $new_path, $overwrite);
        }
        elseif(is_dir($current_path)) {
            $this->recursiveDirectoryCopy($current_path, $new_path, $overwrite);
        }
        else {
            throw new \InvalidArgumentException('Current path (' 
                    . $current_path . ') does not exist.');
        }
    }
    
    
    /**
     * Delete a directory on the file system by its path.
     * 
     * @param string $directory_path (may be nested)
     * @param bool $throw_exception_if_directory_not_found if true a 
     *  \League\Flysystem\FileNotFoundException will be thrown if the file is 
     *  not found
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 07/09/2017
     */
    public function deleteDirectory($directory_path, $throw_exception_if_directory_not_found = true) {
        $directory_path = $this->validatePath($directory_path, $throw_exception_if_directory_not_found);
        if(! $throw_exception_if_directory_not_found && ! $directory_path) {
            return $this;
        }
        $this->recursiveDirectoryDelete($directory_path);        
        return $this;
    }
    
    
    /**
     * Recursively copy a directory and its contents.
     * 
     * @param string $source directory source path
     * @param string $destination directory destination path
     * @param bool $overwrite if true the destination directory will be 
     *  deleted if it exists
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 07/09/2017
     */
    protected function recursiveDirectoryCopy($source, $destination, $overwrite = true) {
        if(!is_dir($destination)) {
            if($overwrite) {
                $this->recursiveDirectoryDelete($destination);
            }
            mkdir($destination, 0755);
        }
        foreach (
            $iterator = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST) as $item) {
            if ($item->isDir()) {
                mkdir($destination . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            } else {
                copy($item, $destination . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            }
        }
    }
    
    
    /**
     * Recursively remove a directory and its contents.
     * 
     * @param string $path the path to the directory
     * @return void
     * @version 0.1.0 07/09/2017
     * @since 0.1.0 07/09/2017
     */
    protected function recursiveDirectoryDelete($path) {
        if (! is_dir($path)) {
            return;
        }
        if (substr($path, strlen($path) - 1, 1) != '/') {
            $path .= '/';
        }
        $files = glob($path . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($path);
    }
    
    
}
