<?php
/**
 * Wrapper class for communicating with the Dropbox API 
 *  (using thephpleague/flysystem) using PHP 7.x.
 * 
 * This class uses version 2 of the Dropbox API and works on PHP7 or higher.
 * For PHP5 use Dropbox5.php instead.
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 07/12/2017 0.1.0
 * @since 06/29/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com/adapter/dropbox/
 * @link https://github.com/spatie/flysystem-dropbox
 * 
 * Before being able to use this class, you need to create an app and get an access token from Dropbox.
 * @link https://www.dropbox.com/developers/apps/create
 */
namespace Snap\Flysystem;

// import classes
use League\Flysystem\Filesystem as Filesystem;
use Spatie\Dropbox\Client as Client;
use Spatie\FlysystemDropbox\DropboxAdapter as Adapter;

class Dropbox7 extends FlysystemAbstract {
    
    
    /**
     *
     * @var string copy of dropbox auth token
     */
    protected $auth_token;
    
    
    /**
     *
     * @var adapter client object
     */
    protected $Client;
    
    
    /**
     * Constructor performs set up
     * 
     * @param string $auth_token dropbox auth token
     * @param string $file_path the relative file path inside the dropbox folder for the application
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function __construct($auth_token, $file_path = '') {
        // copy the auth token to the object
        $this->auth_token = (string) $auth_token;
        // create the client object
        $this->Client = new Client($auth_token);
        // set the file path and create the file system object
        $this->setFilePath($file_path);
    }
    
    
    /**
     * Build and return a league flysystem Filesystem object for a specified directory within the connected dropbox account.
     * 
     * @param string $file_path optional path to the desired directory within the dropbox account
     * @return \League\Flysystem\Filesystem
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function fileSystemFactory($file_path = '') {
        if(!$this->Client) {
            throw new \UnexpectedValueException('There is no client object set '
                    . 'as a property of ' . $this->get_class());
        }
        return new Filesystem(new Adapter($this->Client, $file_path));
    }
    
    
    /**
     * Return an array of files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     * @version 0.1.0 06/29/2017
     * @since 0.1.0 06/29/2017
     */
    public function getFiles($only_return_names = false) {
        $contents = $this->listContents();
        $files = [];
        if(!$contents) {
            return $files;
        }
        foreach($contents as $file) {
            // skip dropbox.com urls files
            if(strpos($file['path'], 'www.dropbox.com') !== false) {
                continue;
            }
            if($file['type'] === 'file') {
                if($only_return_names) {
                    $files[] = $file['path'];
                }
                else {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }
    
    
    /**
     * Override for Filesystem method because the current Dropbox API does not 
     *  support returning mime types.
     * 
     * @param string $path
     * @throws \ErrorException
     * @version 06/30/2017 0.1.0
     * @since 06/29/2017 0.1.0
     */
    public function getMimetype($path) {
        throw new \ErrorException('The Dropbox API version 2 does not support '
                . 'mime types.');
    }    

    
}
