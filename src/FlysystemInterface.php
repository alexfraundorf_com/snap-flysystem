<?php
/**
 * An interface implemented by all final classes the may also extend 
 * FlysystemAbstract.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2017, Alex Fraundorf and/or Snap Programming and Development LLC
 * @version 06/30/2017 0.1.0
 * @since 06/29/2017 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * @link https://flysystem.thephpleague.com
 * @link https://github.com/thephpleague/flysystem/tree/master/src
 */
namespace Snap\Flysystem;

interface FlysystemInterface {

    
    /**
     * Return the filesystem object.
     * 
     * @return League\Flysystem\FilesystemInterface
     */
    public function getFileSystemObject();
    
    
    /**
     * Return the adapter object.
     * 
     * @return League\Flysystem\AdapterInterface
     */
    public function getAdapterObject();
        
    
    /**
     * Sets/updates the file path and the object's FileSystem object.
     * 
     * Note: If file path begins with a / it will be absolute to the file system's 
     *  root.  If not, it will be treated as relative to the existing file 
     *  path ie: drilling down.
     * 
     * @param string $file_path new file path (relative to the application's dropbox folder)
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \InvalidArgumentException
     * @throws \ErrorException
     */
    public function setFilePath($file_path = '/');
    
    
    /**
     * Return the current file path.
     * 
     * @return string
     */
    public function getFilePath();
    
    
    /**
     * Return an array of directories in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     */
    public function getDirectories($only_return_names = false);
    
    
    /**
     * Return an array of files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     */
    public function getFiles($only_return_names = false);
    
    
    /**
     * Return an array of the image files in the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     */
    public function getImageFiles($only_return_names = false);
    
    
    /**
     * Return a multi-dimensional array with sub-arrays of 'directories' and 'files' 
     *  containing an array of directories and files, respectively, within the current path.
     * 
     * @param bool $only_return_names if true only the file names will be returned
     * @return array
     */
    public function getDirectoriesAndFiles($only_return_names = false);
    
    
    /**
     * Return an array of details about a file/directory by its name or an empty 
     *  array if no match is found.
     * 
     * @param string $file_name
     * @return array
     * @throws \League\Flysystem\FileNotFoundException if file is not found
     */
    public function getFileDetails($file_name);
    
    
    /**
     * Copy a file from the current file system file path to a local file path.
     * 
     * The object will be returned on success and an exception will be thrown if 
     *  there is an error.
     * 
     * @param string $remote_file_name the name of the file within the current file path
     * @param string $local_file_path local file path (tip: use an absolute path)
     *   If an absolute path is not used, it will be relative to the file's location.
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract
     * @throws \ErrorException
     * @throws \UnexpectedValueException
     */
    public function copyFileFromFileSystem($remote_file_name, $local_file_path, $overwrite = true);
    
    
    /**
     * Copy a file from a local path to the file system.
     * 
     * @param string $local_file_path local file path (tip: use an absolute path)
     *   If an absolute path is not used, it will be relative to the file's location.
     * @param string $remote_file_name the new name of the file being copied to 
     *  the remote file system
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem/FlysystemAbstract
     * @throws \UnexpectedValueException
     * @throws \ErrorException
     */
    public function copyFileToFileSystem($local_file_path, $remote_file_name, $overwrite = true);
     
    
    /**
     * Create a directory.
     *
     * @param string $directory_name the name of the new directory
     *  Can be "test" or nested "test/test2/test3"
     * @param array  $config  an optional configuration array
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException if the directory was not created
     */
    public function createDirectory($directory_name, array $config = []);
    
    
    /**
     * Delete a file on the file system by its name/path.
     * 
     * @param string $file_path
     * @return \Snap\Flysystem\FlysystemAbstract
     * @throws \League\Flysystem\FileNotFoundException if file can't be found
     * @throws \ErrorException of delete fails
     */
    public function deleteFile($file_path);
    
    
    /**
     * Delete a directory on the file system by its path.
     * 
     * @param string $directory_path (may be nested)
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \ErrorException
     */
    public function deleteDirectory($directory_path);
    
    
    /**
     * Copy a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if copy fails
     */
    public function copy($current_path, $new_path, $overwrite = true);
    
    
    /**
     * Move a file or directory (including its contents) on a file system.
     * 
     * Note: Both the current path and new path must be on the same file system.
     * 
     * @param string $current_path path of source to copy
     * @param string $new_path destination path to copy to
     * @param bool $overwrite if true (default) the file will be overwritten if 
     *  it already exists - if false and the file exists, an exception will be thrown
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \UnexpectedValueException if the destination already exists and 
     *  $overwrite is false
     * @throws \ErrorException if move fails
     */
    public function move($current_path, $new_path, $overwrite = true);
    
    
    /**
     * Recursively copy a directory and all of its contents from file system.
     * 
     * @param string $path_to_remote_directory path to the remote file system 
     *  directory to copy
     * @param string $path_to_local_directory destination path to copy to. 
     *  tip: use an absolute path
     * @return \Snap\Flysystem\FlysystemAbstract on success
     * @throws \League\Flysystem\FileNotFoundException if the file/directory to 
     *  be copied cannot be found
     * @throws \ErrorException if a copy error occurs
     */
    public function copyDirectoryFromFileSystem($path_to_remote_directory, $path_to_local_directory);
    
    
    
    
    
    
    
    
    
    
    // untested
    public function copyDirectoryToFileSystem($path_to_local_directory, $path_to_remote_directory);
    
    
    
    
    
}
